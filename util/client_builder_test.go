package util

import (
	"context"
	"testing"

	logrTesting "github.com/go-logr/logr/testing"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	clusterregistryv1alpha1 "ake.alauda.io/cluster-registry/api/v1alpha1"
)

func TestBuildDiscoveryClientFromConfig(t *testing.T) {
	t.Run("Right cluster config", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}

		c := buildSimpleCluster("https://127.0.0.1", []byte("ca content"), nil)
		_, err := BuildDiscoveryClientFromConfig(logger, c)
		assertNoError(t, err)
	})

	t.Run("Right cluster config without caBundle", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}

		c := buildSimpleCluster("https://127.0.0.1", []byte{}, nil)
		_, err := BuildDiscoveryClientFromConfig(logger, c)
		assertNoError(t, err)
	})

	t.Run("Bad cluster config without server address", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}

		c := buildSimpleCluster("", []byte("ca content"), nil)
		_, err := BuildDiscoveryClientFromConfig(logger, c)
		assertError(t, err, ErrNoAvailableServerEndpoint)
	})
}

func TestBuildClientsetFromConfig(t *testing.T) {
	t.Run("Right auth secret reference and secret", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}

		goodSecretRef := &clusterregistryv1alpha1.ObjectReference{
			Kind: "Secret", Name: "test_secret", Namespace: "default",
		}
		client := fake.NewFakeClient(&corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test_secret",
				Namespace: "default",
			},
			Data: map[string][]byte{
				TokenKey: []byte("cluster token"),
			},
		})

		c := buildSimpleCluster("https://127.0.0.1", []byte("ca content"), goodSecretRef)
		_, err := BuildClientsetFromConfig(context.TODO(), logger, client, c)
		assertNoError(t, err)
	})

	t.Run("Bad auth secret reference without namespace", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}

		badSecretRef := &clusterregistryv1alpha1.ObjectReference{
			Kind: "Secret", Name: "test_secret",
		}
		client := fake.NewFakeClient()

		c := buildSimpleCluster("https://127.0.0.1", []byte("ca content"), badSecretRef)
		_, err := BuildClientsetFromConfig(context.TODO(), logger, client, c)
		assertError(t, err, ErrAuthSecretNotProvided)
	})

	t.Run("Secret not exist", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}

		secretRefToNoexistSecret := &clusterregistryv1alpha1.ObjectReference{
			Kind: "Secret", Name: "no_exist_secret", Namespace: "default",
		}
		client := fake.NewFakeClient(&corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test_secret",
				Namespace: "default",
			},
		})

		c := buildSimpleCluster("https://127.0.0.1", []byte("ca content"), secretRefToNoexistSecret)
		_, err := BuildClientsetFromConfig(context.TODO(), logger, client, c)
		assertError(t, err, ErrAuthSecretFetchFailed)
	})

	t.Run("Secret data doesn't have token field", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}

		secretRefToInvalidSecret := &clusterregistryv1alpha1.ObjectReference{
			Kind: "Secret", Name: "invalid_secret", Namespace: "default",
		}
		client := fake.NewFakeClient(&corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "invalid_secret",
				Namespace: "default",
			},
		})

		c := buildSimpleCluster("https://127.0.0.1", []byte("ca content"), secretRefToInvalidSecret)
		_, err := BuildClientsetFromConfig(context.TODO(), logger, client, c)
		assertError(t, err, ErrAuthSecretInvalid)
	})
}

func assertError(t *testing.T, got error, want error) {
	t.Helper()

	if got == nil {
		t.Fatal("wanted an error but didn't get one")
	}

	if got != want {
		t.Errorf("got %s, want %s", got, want)
	}
}

func assertNoError(t *testing.T, got error) {
	t.Helper()

	if got != nil {
		t.Fatal("didn't want an error but got one")
	}
}

func buildSimpleCluster(serverAddress string, caBundle []byte, secretRef *clusterregistryv1alpha1.ObjectReference) *clusterregistryv1alpha1.Cluster {
	c := &clusterregistryv1alpha1.Cluster{}
	if serverAddress != "" {
		c.Spec.KubernetesAPIEndpoints.ServerEndpoints = []clusterregistryv1alpha1.ServerAddressByClientCIDR{
			{ServerAddress: serverAddress},
		}
	}
	if len(caBundle) != 0 {
		c.Spec.KubernetesAPIEndpoints.CABundle = caBundle
	}
	if secretRef != nil {
		c.Spec.AuthInfo.Controller = secretRef
	}
	return c
}
