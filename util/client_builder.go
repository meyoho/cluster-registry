package util

import (
	"context"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"github.com/go-logr/logr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	clusterregistryv1alpha1 "ake.alauda.io/cluster-registry/api/v1alpha1"
)

const (
	TokenKey = "token"

	ErrNoAvailableServerEndpoint = BuildClientErr("could not found an avaliable server endpoint from cluster config.")
	ErrAuthSecretNotProvided     = BuildClientErr("Secret Reference is not provided fully in the cluster config.")
	ErrAuthSecretFetchFailed     = BuildClientErr("Failed to fetch the secret resource specified in cluster config.")
	ErrAuthSecretInvalid         = BuildClientErr("Auth secret data is invalid, no token field found.")
)

type BuildClientErr string

func (e BuildClientErr) Error() string {
	return string(e)
}

func BuildDiscoveryClientFromConfig(logger logr.Logger, c *clusterregistryv1alpha1.Cluster) (*discovery.DiscoveryClient, error) {
	logger.Info("begin to build discovery client")

	restConfig, err := buildBasicRestConfig(logger, c)
	if err != nil {
		return &discovery.DiscoveryClient{}, err
	}

	discoveryClient, err := discovery.NewDiscoveryClientForConfig(restConfig)
	return discoveryClient, err
}

func BuildClientsetFromConfig(ctx context.Context, logger logr.Logger, client client.Client, c *clusterregistryv1alpha1.Cluster) (*kubernetes.Clientset, error) {
	logger.Info("begin to build clientset")

	restConfig, err := buildBasicRestConfig(logger, c)
	if err != nil {
		return &kubernetes.Clientset{}, err
	}

	if c.Spec.AuthInfo.Controller == nil || c.Spec.AuthInfo.Controller.Name == "" ||
		c.Spec.AuthInfo.Controller.Namespace == "" {
		return &kubernetes.Clientset{}, ErrAuthSecretNotProvided
	}

	secret := &corev1.Secret{}
	err = client.Get(ctx,
		types.NamespacedName{
			Namespace: c.Spec.AuthInfo.Controller.Namespace,
			Name:      c.Spec.AuthInfo.Controller.Name,
		},
		secret)
	if err != nil {
		logger.Error(err, "unable to fetch secret specified in cluster")
		return &kubernetes.Clientset{}, ErrAuthSecretFetchFailed
	}

	token, exist := secret.Data[TokenKey]
	if !exist {
		return &kubernetes.Clientset{}, ErrAuthSecretInvalid
	}

	restConfig.BearerToken = string(token[:])
	clientSet, err := kubernetes.NewForConfig(restConfig)
	return clientSet, err
}

func buildBasicRestConfig(_ logr.Logger, c *clusterregistryv1alpha1.Cluster) (*rest.Config, error) {
	if len(c.Spec.KubernetesAPIEndpoints.ServerEndpoints) == 0 || c.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ServerAddress == "" {
		return &rest.Config{}, ErrNoAvailableServerEndpoint
	}

	config := &rest.Config{
		Host: c.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ServerAddress,
	}
	if len(c.Spec.KubernetesAPIEndpoints.CABundle) == 0 {
		config.TLSClientConfig.Insecure = true
	} else {
		config.TLSClientConfig.CAData = c.Spec.KubernetesAPIEndpoints.CABundle
	}

	return config, nil
}
