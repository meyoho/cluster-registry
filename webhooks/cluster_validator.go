package webhooks

import (
	"context"
	"net/http"

	"github.com/go-logr/logr"
	admissionv1beta1 "k8s.io/api/admission/v1beta1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"

	adapter "ake.alauda.io/cluster-registry/adapter"
	clusterregistryv1alpha1 "ake.alauda.io/cluster-registry/api/v1alpha1"
	core "ake.alauda.io/cluster-registry/domain/entity"
	uc "ake.alauda.io/cluster-registry/domain/usecase"
	util "ake.alauda.io/cluster-registry/util"
)

const (
	ClusterClientInvalid   = "ClusterClientInvalid"
	ClusterVersionMismatch = "ClusterVersionMismatch"
	ClusterAlreadyImported = "ClusterAlreadyImported"
	ClusterNotImportable   = "ClusterNotImportable"
	ClusterMarkFailed      = "ClusterMarkFailed"
)

type ClusterValidator struct {
	client            client.Client
	decoder           *admission.Decoder
	Log               logr.Logger
	SupportedVersions []core.ClusterVersion
}

// +kubebuilder:webhook:groups=clusterregistry.k8s.io,versions=v1alpha1,resources=clusters,verbs=create;delete,name=validate-cluster.clusterregistry.k8s.io,path=/validate-cluster,mutating=false,failurePolicy=fail

func (v *ClusterValidator) Handle(ctx context.Context, req admission.Request) admission.Response {

	if req.Operation == admissionv1beta1.Create {
		v.Log.Info("validating cluster create request")

		cluster := &clusterregistryv1alpha1.Cluster{}

		err := v.decoder.Decode(req, cluster)
		if err != nil {
			return admission.Errored(http.StatusBadRequest, err)
		}

		discoveryClient, err := util.BuildDiscoveryClientFromConfig(v.Log, cluster)
		if err != nil {
			v.Log.Error(err, "unable to build discoveryClient")
			return admission.Denied(ClusterClientInvalid)
		}
		clienset, err := util.BuildClientsetFromConfig(ctx, v.Log, v.client, cluster)
		if err != nil {
			v.Log.Error(err, "unable to build clientset")
			return admission.Denied(ClusterClientInvalid)
		}

		statusIteractor := uc.StatusInteractor{Log: v.Log,
			Client: &adapter.ClusterClient{
				Log:             v.Log,
				DiscoveryClient: discoveryClient,
				Clientset:       clienset,
			},
		}

		_, err = statusIteractor.CheckImportable(v.SupportedVersions)
		if err == uc.ClusterVersionNotSupported {
			return admission.Denied(ClusterVersionMismatch)
		}
		if err == uc.ClusterAlreadyImported {
			return admission.Denied(ClusterAlreadyImported)
		}
		if err != nil {
			v.Log.Error(err, "cluster not importable")
			return admission.Denied(ClusterNotImportable)
		}

		if err := statusIteractor.MarkAsImported(); err != nil {
			v.Log.Error(err, "mark cluster imported failed.")
			return admission.Denied(ClusterMarkFailed)
		}

		return admission.Allowed("")
	}

	if req.Operation == admissionv1beta1.Delete {
		v.Log.Info("validating cluster delete request")

		cluster := &clusterregistryv1alpha1.Cluster{}
		if err := v.client.Get(
			ctx, types.NamespacedName{Namespace: req.Namespace, Name: req.Name}, cluster); err != nil {
			v.Log.Error(err, "unable to fetch cluster, ignoring it...")
			return admission.Allowed("")
		}

		discoveryClient, err := util.BuildDiscoveryClientFromConfig(v.Log, cluster)
		if err != nil {
			v.Log.Error(err, "unable to build discovery client from cluster, ignoring it...")
			return admission.Allowed("")
		}
		clienset, err := util.BuildClientsetFromConfig(ctx, v.Log, v.client, cluster)
		if err != nil {
			v.Log.Error(err, "unable to build clientset from cluster, ignoring it...")
			return admission.Allowed("")
		}

		statusIteractor := uc.StatusInteractor{Log: v.Log,
			Client: &adapter.ClusterClient{
				Log:             v.Log,
				DiscoveryClient: discoveryClient,
				Clientset:       clienset,
			},
		}

		if err := statusIteractor.DeleteImportedMarker(); err != nil {
			v.Log.Error(err, "delete imported mark failed, ignoring it...")
			return admission.Allowed("")
		}

		return admission.Allowed("")
	}

	return admission.Allowed("")
}

// clusterValidator implements inject.Client.
// A client will be automatically injected.
func (v *ClusterValidator) InjectClient(c client.Client) error {
	v.client = c
	return nil
}

// clusterValidator implements admission.DecoderInjector.
// A decoder will be automatically injected.
func (v *ClusterValidator) InjectDecoder(d *admission.Decoder) error {
	v.decoder = d
	return nil
}
