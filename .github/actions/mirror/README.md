> this action is copied from https://github.com/spyoungtech/mirror-action , and modified the envvar's name.

# mirror-action
A GitHub Action for mirroring your commits to a different remote repository

## Example workflows

### Mirror a repository with username/password over HTTPS

For example, this project uses the following workflow to mirror from GitHub to GitLab

```workflow
name: Sync Code
on: push

jobs:
  sync_code:
    name: sync code
    runs-on: ubuntu-latest
    steps:
    - uses: actions/checkout@v1
    - name: run mirror action
      uses: ./.github/actions/mirror
      with:
        target_repo: https://<code-repo_addr>.git
        target_branch: master
        git_username: ${{ secrets.MIRROR_GIT_USERNAME }}
        git_password: ${{ secrets.MIRROR_GIT_PASSWORD }}
```

Be sure to set the `GIT_PASSWORD` secret in the Actions editor.

### Mirror a repository using SSH

*Coming soon*
