#!/bin/sh

set -eu

sh -c "git config --global credential.username $INPUT_GIT_USERNAME"
sh -c "git config --global core.askPass /cred-helper.sh"
sh -c "git config --global credential.helper cache"
sh -c "git remote add mirror $*"
sh -c "echo pushing to $INPUT_TARGET_BRANCH branch at $(git remote get-url --push mirror)"
sh -c "git checkout $INPUT_TARGET_BRANCH"
sh -c "git push -f mirror $INPUT_TARGET_BRANCH"