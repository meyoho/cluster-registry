/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	clusterregistryv1alpha1 "ake.alauda.io/cluster-registry/api/v1alpha1"
	"ake.alauda.io/cluster-registry/controllers"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/version"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	// +kubebuilder:scaffold:imports

	"ake.alauda.io/cluster-registry/webhooks"
	"sigs.k8s.io/controller-runtime/pkg/webhook"

	core "ake.alauda.io/cluster-registry/domain/entity"
)

const (
	appVersion           = "v0.2.0"
	hookServerCertDir    = "/tmp/k8s-webhook-server/serving-certs"
	supportedVersionsEnv = "SUPPORTED_VERSIONS"
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")

	defaultSupportedVersions = []core.ClusterVersion{
		core.ClusterVersion{Major: 1, Minor: 13},
		core.ClusterVersion{Major: 1, Minor: 14},
	}
)

func init() {
	_ = clientgoscheme.AddToScheme(scheme)

	_ = clusterregistryv1alpha1.AddToScheme(scheme)
	// +kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var printVersion bool
	flag.StringVar(&metricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "enable-leader-election", false,
		"Enable leader election for controller manager. Enabling this will ensure there is only one active controller manager.")
	flag.BoolVar(&printVersion, "v", false, "Show version and exit.")
	flag.Parse()

	if printVersion {
		fmt.Println(appVersion)
		os.Exit(0)
	}

	ctrl.SetLogger(zap.New(func(o *zap.Options) {
		o.Development = true
	}))

	supportedVersions := []core.ClusterVersion{}
	supportedVersionsStr := os.Getenv(supportedVersionsEnv)
	if supportedVersionsStr != "" {
		for _, verStr := range strings.Split(supportedVersionsStr, ",") {
			parsedV, err := version.ParseGeneric(verStr)
			if err != nil {
				setupLog.Error(err, "versions in $SUPPORTED_VERSIONS invalid.")
				supportedVersions = defaultSupportedVersions
				break
			}
			supportedVersions = append(supportedVersions,
				core.ClusterVersion{Major: parsedV.Major(), Minor: parsedV.Minor()})
		}
	} else {
		supportedVersions = defaultSupportedVersions
	}
	setupLog.Info("using supportedVersions", "versions", supportedVersions)

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:             scheme,
		MetricsBindAddress: metricsAddr,
		LeaderElection:     enableLeaderElection,
		LeaderElectionID:   "cluster-registry-lock",
		Port:               9443,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if err = (&controllers.ClusterReconciler{
		Client: mgr.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("Cluster"),
		Scheme: mgr.GetScheme(),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Cluster")
		os.Exit(1)
	}
	// +kubebuilder:scaffold:builder

	// Setup Webhooks
	setupLog.Info("setting up webhook server")
	hookServer := &webhook.Server{
		Port:    9443,
		CertDir: hookServerCertDir,
	}
	if err := mgr.Add(hookServer); err != nil {
		setupLog.Error(err, "unable to register webhook server with manager.")
		os.Exit(1)
	}

	setupLog.Info("registering webhooks to webhook server")
	hookServer.Register("/validate-cluster", &webhook.Admission{Handler: &webhooks.ClusterValidator{
		Log:               ctrl.Log.WithName("validators").WithName("Cluster"),
		SupportedVersions: supportedVersions,
	}})

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
