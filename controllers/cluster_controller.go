/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"time"

	"github.com/go-logr/logr"
	apierrs "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	adapter "ake.alauda.io/cluster-registry/adapter"
	clusterregistryv1alpha1 "ake.alauda.io/cluster-registry/api/v1alpha1"
	uc "ake.alauda.io/cluster-registry/domain/usecase"
	util "ake.alauda.io/cluster-registry/util"
)

var (
	shortRequeueTime, _ = time.ParseDuration("15s")
	longRequeueTime, _  = time.ParseDuration("3m")
)

// ClusterReconciler reconciles a Cluster object
type ClusterReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch
// +kubebuilder:rbac:groups=clusterregistry.k8s.io,resources=clusters,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=clusterregistry.k8s.io,resources=clusters/status,verbs=get;update;patch

func (r *ClusterReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	log := r.Log.WithValues("cluster", req.NamespacedName)

	// Fetch the Cluster instance
	cluster := &clusterregistryv1alpha1.Cluster{}
	if err := r.Get(ctx, req.NamespacedName, cluster); err != nil {
		log.Error(err, "unable to fetch cluster")
		return ctrl.Result{}, ignoreNotFound(err)
	}

	// if build client from config failed then requeue after a long time
	discoveryClient, err := util.BuildDiscoveryClientFromConfig(log, cluster)
	if err != nil {
		log.Error(err, "build discoveryClient failed")
		return ctrl.Result{RequeueAfter: longRequeueTime}, nil
	}
	clienset, err := util.BuildClientsetFromConfig(ctx, log, r.Client, cluster)
	if err != nil {
		log.Error(err, "build clientset failed")
		return ctrl.Result{RequeueAfter: longRequeueTime}, nil
	}

	statusIteractor := uc.StatusInteractor{Log: log,
		Client: &adapter.ClusterClient{
			Log:             log,
			DiscoveryClient: discoveryClient,
			Clientset:       clienset,
		},
	}

	observedStatus := statusIteractor.ObserveStatus()
	current := metav1.Now()
	clusterCopy := cluster.DeepCopy()
	clusterCopy.Status = adapter.PresentClusterStatus(log, observedStatus, current)

	log.Info("updating cluster's status", "new status", clusterCopy.Status)
	// requeue immediately if update status failed
	if err := r.Status().Update(ctx, clusterCopy); err != nil {
		log.Error(err, "unable to update cluster status")
		return ctrl.Result{}, err
	}

	if !observedStatus.Accessible || !observedStatus.ComponentsSummary.AllHealthy ||
		!observedStatus.NodesSummary.AllReady {
		log.Info("cluster is unhealthy", "observed status", observedStatus)
		return ctrl.Result{RequeueAfter: shortRequeueTime}, nil
	} else {
		log.Info("cluster is healthy")
		return ctrl.Result{RequeueAfter: longRequeueTime}, nil
	}

}

func (r *ClusterReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&clusterregistryv1alpha1.Cluster{}).
		Complete(r)
}

func ignoreNotFound(err error) error {
	if apierrs.IsNotFound(err) {
		return nil
	}
	// Error reading the object - requeue the request.
	return err
}
