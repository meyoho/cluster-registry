# Image URL to use all building/pushing image targets
IMG ?= cluster-registry
# Produce CRDs that work back to Kubernetes 1.11 (no version conversion)
CRD_OPTIONS ?= "crd:trivialVersions=true"

ENV_SUFFIX ?= -local
PLATFORM_VERSION ?= "2.0"

CLUSTER_NAME ?= cluster-for-cluster-registry

# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif

all: manager

# Run unit tests
unit: generate fmt vet
	bash hack/cover.sh README.md

# Run integration tests
int: unit manifests
	go test ./... -cover

# Run tests
test: generate fmt vet manifests unit int

# Build manager binary
manager: generate fmt vet
	go build -o bin/manager main.go

# Run against the configured Kubernetes cluster in ~/.kube/config
run: generate fmt vet manifests
	go run ./main.go

# Install CRDs and webhook manifest into a cluster
install: CABUNDLE = $(shell cat /tmp/k8s-webhook-server/serving-certs/rootCA.pem|base64 -w 0)
install: manifests certs local_ip
	kustomize build config/crd | kubectl apply -f -
	bash hack/generate_local_webhook_manifest.sh $(LOCAL_IP) $(HOOK_SERVER_PORT) $(CABUNDLE) | kubectl apply -f -

# Uninstall CRDs from a cluster
uninstall: manifests
	kustomize build config/crd | kubectl delete -f -

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests
	kubectl apply -f config/crd/bases
	kustomize build config/overlays/dev | kubectl apply -f -

# Deploy product controller in the configured Kubernetes cluster in ~/.kube/config
deploy-prod: manifests
	kubectl apply -f config/crd/bases
	kustomize build config/overlays/prod | kubectl apply -f -

# Generate helm chart from config/default kustomization
helmify: version manifests
	bash hack/helmify.sh build $(PLATFORM_VERSION) $(APP_VERSION)

# Generate manifests e.g. CRD, RBAC etc.
manifests: controller-gen
	$(CONTROLLER_GEN) $(CRD_OPTIONS) rbac:roleName=manager-role webhook paths="./..." output:crd:artifacts:config=config/crd/bases

# Generate local certs for wenhook server
certs: export CAROOT = /tmp/k8s-webhook-server/serving-certs
certs: local_ip
	@echo "generating certs for $(LOCAL_IP)"
	mkcert -install
	cd $(CAROOT) && mkcert -cert-file tls.crt -key-file tls.key localhost 127.0.0.1 $(LOCAL_IP)

# Cleanup intermediate artifacts
clean:
	bash hack/helmify.sh clean
	rm -rf bin
	rm -rf cover
	rm -rf /tmp/k8s-webhook-server/

# Setup a kind cluster and deploy cert-manager
cluster-up: export KUBECONFIG = ${HOME}/.kube/kind-config-${CLUSTER_NAME}
cluster-up:
	kind create cluster --name $(CLUSTER_NAME) || true
	kubectl create namespace cert-manager --dry-run -o json | kubectl apply -f -
	kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true --dry-run -o json | kubectl apply -f -
	kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v0.8.1/cert-manager.yaml
	kubectl get all -n cert-manager
	@echo "🛎️ Cluster set up. You can now use the cluster with:\nexport KUBECONFIG=$(shell kind get kubeconfig-path --name=${CLUSTER_NAME})"

# Tear down the kind cluster
cluster-down: export KUBECONFIG = ${HOME}/.kube/kind-config-${CLUSTER_NAME}
cluster-down:
	kind delete cluster --name $(CLUSTER_NAME)

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
vet:
	go vet ./...

# Generate code
generate: controller-gen
	$(CONTROLLER_GEN) object:headerFile=./hack/boilerplate.go.txt paths="./..."

# Build the docker image
docker-build: version
	docker build . -t ${IMG}:$(APP_VERSION)$(ENV_SUFFIX)
	docker tag ${IMG}:$(APP_VERSION)$(ENV_SUFFIX) ${IMG}:latest$(ENV_SUFFIX)

# Push the docker image
docker-push: version
	docker push ${IMG}:$(APP_VERSION)$(ENV_SUFFIX)

# Load docker images into kind cluster
load-images: version
	kind load docker-image ${IMG}:latest$(ENV_SUFFIX) --name $(CLUSTER_NAME)

# Set application version var
version:
APP_VERSION=$(shell go run main.go -v)

# find or download controller-gen
# download controller-gen if necessary
controller-gen:
ifeq (, $(shell which controller-gen))
	@{ \
	set -e ;\
	CONTROLLER_GEN_TMP_DIR=$$(mktemp -d) ;\
	cd $$CONTROLLER_GEN_TMP_DIR ;\
	go mod init tmp ;\
	go get sigs.k8s.io/controller-tools/cmd/controller-gen@v0.2.1 ;\
	rm -rf $$CONTROLLER_GEN_TMP_DIR ;\
	}
CONTROLLER_GEN=$(GOBIN)/controller-gen
else
CONTROLLER_GEN=$(shell which controller-gen)
endif

# Set local ip
local_ip:
LOCAL_IP=$(shell ./hack/print_local_ip.sh)