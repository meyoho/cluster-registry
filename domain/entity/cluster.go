package entity

import (
	"fmt"
)

type ClusterVersion struct {
	Major uint
	Minor uint
	Patch uint
}

func (v *ClusterVersion) String() string {
	return fmt.Sprintf("v%d.%d.%d", v.Major, v.Minor, v.Patch)
}

type ComponentStatus struct {
	Name    string
	Healthy bool
	Message string
}

type NodeStatus struct {
	Name    string
	Ready   bool
	Message string
}

type ComponentsSummary struct {
	AllHealthy bool
	Reason     StatusReason
	Message    string
}

type NodesSummary struct {
	AllReady bool
	Reason   StatusReason
	Message  string
}

type StatusReason string

const (
	StatusUnknown           = StatusReason("status_unknown")
	AllComponentsHealthy    = StatusReason("all_components_healthy")
	SomeComponentsUnhealthy = StatusReason("some_somponents_unhealthy")
	AllNodesReady           = StatusReason("all_nodes_ready")
	SomeNodesNotReady       = StatusReason("some_nodes_not_ready")
)

func IsVersionSupported(target ClusterVersion, supported []ClusterVersion) bool {
	for _, v := range supported {
		if target.Major == v.Major && target.Minor == v.Minor {
			return true
		}
	}
	return false
}
