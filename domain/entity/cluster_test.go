package entity

import (
	"testing"
)

func TestClusterVersion(t *testing.T) {
	version := &ClusterVersion{Major: 1, Minor: 13, Patch: 4}

	if version.String() != "v1.13.4" {
		t.Errorf("wanted %s but got %s", "v1.13.4", version.String())
	}
}

func TestIsVersionSupported(t *testing.T) {
	versionTests := []struct {
		name      string
		target    [3]uint
		supported [][3]uint
		want      bool
	}{
		{
			name:      "patch miss matched should return true",
			target:    [3]uint{1, 13, 3},
			supported: [][3]uint{[3]uint{1, 13, 2}},
			want:      true,
		},
		{
			name:      "minor miss matched should return false",
			target:    [3]uint{1, 13, 3},
			supported: [][3]uint{[3]uint{1, 12, 3}},
			want:      false,
		},
		{
			name:      "major miss matched should return false",
			target:    [3]uint{1, 13, 3},
			supported: [][3]uint{[3]uint{2, 13, 3}},
			want:      false,
		},
		{
			name:      "only one matched should return true",
			target:    [3]uint{1, 13, 3},
			supported: [][3]uint{[3]uint{1, 13, 3}, [3]uint{1, 12, 3}},
			want:      true,
		},
	}

	for _, tt := range versionTests {
		t.Run(tt.name, func(t *testing.T) {
			targetVersion := ClusterVersion{Major: tt.target[0], Minor: tt.target[1], Patch: tt.target[2]}
			supportedVersions := []ClusterVersion{}
			for _, v := range tt.supported {
				supportedVersions = append(supportedVersions,
					ClusterVersion{Major: v[0], Minor: v[1], Patch: v[2]})
			}

			got := IsVersionSupported(targetVersion, supportedVersions)

			if got != tt.want {
				t.Errorf("wanted %v but got %v", tt.want, got)
			}
		})
	}
}
