package usecase

import (
	"errors"
	"reflect"
	"testing"

	core "ake.alauda.io/cluster-registry/domain/entity"
	logrTesting "github.com/go-logr/logr/testing"
)

type ClientSpy struct {
	Calls []string

	version                core.ClusterVersion
	namespaceLabels        map[string]string
	componentStatusList    []core.ComponentStatus
	nodeStatusList         []core.NodeStatus
	getVersionErr          error
	getNamespaceLabelsErr  error
	listComponentStatusErr error
	listNodeStatusErr      error
	setNamespaceLabelsErr  error
}

const (
	getVersion          = "GetVersion"
	getNamespaceLabels  = "GetNamespaceLabels"
	listComponentStatus = "ListComponentStatus"
	listNodeStatus      = "ListNodeStatus"
	setNamespaceLabels  = "SetNamespaceLabels"
)

func (s *ClientSpy) GetVersion() (core.ClusterVersion, error) {
	s.Calls = append(s.Calls, getVersion)
	return s.version, s.getVersionErr
}

func (s *ClientSpy) GetNamespaceLabels(namespace string) (map[string]string, error) {
	s.Calls = append(s.Calls, getNamespaceLabels)
	return s.namespaceLabels, s.getNamespaceLabelsErr
}

func (s *ClientSpy) ListComponentStatus() ([]core.ComponentStatus, error) {
	s.Calls = append(s.Calls, listComponentStatus)
	return s.componentStatusList, s.listComponentStatusErr
}

func (s *ClientSpy) ListNodeStatus() ([]core.NodeStatus, error) {
	s.Calls = append(s.Calls, listNodeStatus)
	return s.nodeStatusList, s.listNodeStatusErr
}

func (s *ClientSpy) SetNamespaceLabels(namespace string, labels map[string]string) error {
	s.Calls = append(s.Calls, setNamespaceLabels)
	if s.setNamespaceLabelsErr == nil {
		s.namespaceLabels = labels
	}
	return s.setNamespaceLabelsErr
}

func TestCheckImportable(t *testing.T) {
	t.Run("make sure getNamespaceLabels is called", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		clientSpy := &ClientSpy{}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		_, _ = i.CheckImportable([]core.ClusterVersion{core.ClusterVersion{}})

		assertWantedCalls(t, []string{getVersion, getNamespaceLabels}, clientSpy)
	})

	t.Run("get version failed", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		getVersionErr := errors.New("get version failed")
		clientSpy := &ClientSpy{getVersionErr: getVersionErr}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := false

		got, err := i.CheckImportable([]core.ClusterVersion{core.ClusterVersion{}})

		assertError(t, err, getVersionErr)

		if got != want {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})

	t.Run("get namespace labels failed", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		getNamespaceLabelsErr := errors.New("get namespace labels failed")
		clientSpy := &ClientSpy{getNamespaceLabelsErr: getNamespaceLabelsErr}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := false

		got, err := i.CheckImportable([]core.ClusterVersion{core.ClusterVersion{}})

		assertError(t, err, getNamespaceLabelsErr)

		if got != want {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})

	t.Run("cluster is already imported", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		clientSpy := &ClientSpy{namespaceLabels: map[string]string{markerKey: ""}}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := false

		got, err := i.CheckImportable([]core.ClusterVersion{core.ClusterVersion{}})

		assertError(t, err, ClusterAlreadyImported)

		if got != want {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})

	t.Run("cluster version is not supported", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		clientSpy := &ClientSpy{version: core.ClusterVersion{Major: 1, Minor: 12}}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := false

		got, err := i.CheckImportable([]core.ClusterVersion{core.ClusterVersion{Major: 1, Minor: 13}})

		assertError(t, err, ClusterVersionNotSupported)

		if got != want {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})

	t.Run("cluster is importable", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		version := core.ClusterVersion{Major: 1, Minor: 13}
		clientSpy := &ClientSpy{namespaceLabels: map[string]string{"no-exist-key": ""}, version: version}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := true

		got, err := i.CheckImportable([]core.ClusterVersion{version})

		assertNoError(t, err)

		if got != want {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})
}

func TestMarkAsImported(t *testing.T) {
	t.Run("make sure setNamespaceLabels is called", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		clientSpy := &ClientSpy{}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		_ = i.MarkAsImported()

		assertWantedCalls(t, []string{getNamespaceLabels, setNamespaceLabels}, clientSpy)
	})

	t.Run("get labels failed", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		getNamespaceLabelsErr := errors.New("get namespace labels failed")
		clientSpy := &ClientSpy{setNamespaceLabelsErr: getNamespaceLabelsErr}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		err := i.MarkAsImported()

		assertError(t, err, getNamespaceLabelsErr)
	})

	t.Run("set labels failed", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		setNamespaceLabelsErr := errors.New("set namespace labels failed")
		clientSpy := &ClientSpy{setNamespaceLabelsErr: setNamespaceLabelsErr}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		err := i.MarkAsImported()

		assertError(t, err, setNamespaceLabelsErr)
	})

	t.Run("mark not imported cluster as imported", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		labels := map[string]string{"foo": "bar"}
		clientSpy := &ClientSpy{namespaceLabels: labels}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := map[string]string{
			markerKey: "",
			"foo":     "bar",
		}

		err := i.MarkAsImported()

		assertNoError(t, err)

		if !reflect.DeepEqual(clientSpy.namespaceLabels, want) {
			t.Errorf("wanted %v but got %v", want, clientSpy.namespaceLabels)
		}
	})

	t.Run("mark imported cluster as imported", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		labels := map[string]string{markerKey: "", "foo": "bar"}
		clientSpy := &ClientSpy{namespaceLabels: labels}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := map[string]string{
			markerKey: "",
			"foo":     "bar",
		}

		err := i.MarkAsImported()

		assertNoError(t, err)

		if !reflect.DeepEqual(clientSpy.namespaceLabels, want) {
			t.Errorf("wanted %v but got %v", want, clientSpy.namespaceLabels)
		}
	})
}

func TestDeleteImportedMarker(t *testing.T) {
	t.Run("make sure setNamespaceLabels is called", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		clientSpy := &ClientSpy{}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		_ = i.DeleteImportedMarker()

		assertWantedCalls(t, []string{getNamespaceLabels, setNamespaceLabels}, clientSpy)
	})

	t.Run("get labels failed", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		getNamespaceLabelsErr := errors.New("get namespace labels failed")
		clientSpy := &ClientSpy{setNamespaceLabelsErr: getNamespaceLabelsErr}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		err := i.DeleteImportedMarker()

		assertError(t, err, getNamespaceLabelsErr)
	})

	t.Run("set labels failed", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		setNamespaceLabelsErr := errors.New("set namespace labels failed")
		clientSpy := &ClientSpy{setNamespaceLabelsErr: setNamespaceLabelsErr}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		err := i.DeleteImportedMarker()

		assertError(t, err, setNamespaceLabelsErr)
	})

	t.Run("imported mark not exist", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		labels := map[string]string{"foo": "bar"}
		clientSpy := &ClientSpy{namespaceLabels: labels}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := map[string]string{
			"foo": "bar",
		}

		err := i.DeleteImportedMarker()

		assertNoError(t, err)

		if !reflect.DeepEqual(clientSpy.namespaceLabels, want) {
			t.Errorf("wanted %v but got %v", want, clientSpy.namespaceLabels)
		}
	})

	t.Run("delete imported mark", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		labels := map[string]string{markerKey: "", "foo": "bar"}
		clientSpy := &ClientSpy{namespaceLabels: labels}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := map[string]string{
			"foo": "bar",
		}

		err := i.DeleteImportedMarker()

		assertNoError(t, err)

		if !reflect.DeepEqual(clientSpy.namespaceLabels, want) {
			t.Errorf("wanted %v but got %v", want, clientSpy.namespaceLabels)
		}
	})
}

func TestObserveStatus(t *testing.T) {
	t.Run("cluster is not accessible", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		getVersionFailed := errors.New("get version failed")
		clientSpy := &ClientSpy{getVersionErr: getVersionFailed}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := ObservedStatus{
			Accessible:       false,
			AccessibleErrMsg: getVersionFailed.Error(),
			ComponentsSummary: core.ComponentsSummary{
				Reason:  core.StatusUnknown,
				Message: clusterNotAccessibleMsg,
			},
			NodesSummary: core.NodesSummary{
				Reason:  core.StatusUnknown,
				Message: clusterNotAccessibleMsg,
			},
		}

		got := i.ObserveStatus()

		if got != want {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})

	t.Run("cluster is ok", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		clientSpy := &ClientSpy{
			version: core.ClusterVersion{Major: 1, Minor: 13, Patch: 4},
			componentStatusList: []core.ComponentStatus{
				core.ComponentStatus{Name: "controller", Healthy: true},
				core.ComponentStatus{Name: "etcd", Healthy: true},
			},
			nodeStatusList: []core.NodeStatus{
				core.NodeStatus{Name: "node1", Ready: true},
			},
		}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := ObservedStatus{
			Accessible:        true,
			Version:           "v1.13.4",
			ComponentsSummary: core.ComponentsSummary{AllHealthy: true, Reason: core.AllComponentsHealthy},
			NodesSummary:      core.NodesSummary{AllReady: true, Reason: core.AllNodesReady},
		}

		got := i.ObserveStatus()

		if got != want {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})
}

func TestObserveComponentsStatus(t *testing.T) {
	t.Run("make sure listComponentStatus is called", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		clientSpy := &ClientSpy{}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		_ = i.observeComponentsStatus()

		assertWantedCalls(t, []string{listComponentStatus}, clientSpy)
	})

	t.Run("get cs failed", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		listComponentStatusErr := errors.New("list cs failed")
		clientSpy := &ClientSpy{listComponentStatusErr: listComponentStatusErr}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := core.ComponentsSummary{
			AllHealthy: false,
			Reason:     core.StatusUnknown,
			Message:    listComponentStatusErr.Error(),
		}

		got := i.observeComponentsStatus()

		if !reflect.DeepEqual(got, want) {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})

	t.Run("one component unhealthy", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		ClientSpy := &ClientSpy{componentStatusList: []core.ComponentStatus{
			core.ComponentStatus{Name: "controller", Healthy: true},
			core.ComponentStatus{Name: "etcd", Healthy: false, Message: "etcd is unhealthy"},
			core.ComponentStatus{Name: "apiserver", Healthy: true},
		}}
		i := StatusInteractor{Log: logger, Client: ClientSpy}

		want := core.ComponentsSummary{
			AllHealthy: false,
			Reason:     core.SomeComponentsUnhealthy,
			Message:    "etcd are(is) unhealthy.",
		}

		got := i.observeComponentsStatus()

		if !reflect.DeepEqual(got, want) {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})

	t.Run("all components are healthy", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		ClientSpy := &ClientSpy{componentStatusList: []core.ComponentStatus{
			core.ComponentStatus{Name: "controller", Healthy: true},
			core.ComponentStatus{Name: "etcd", Healthy: true},
			core.ComponentStatus{Name: "apiserver", Healthy: true},
		}}
		i := StatusInteractor{Log: logger, Client: ClientSpy}

		want := core.ComponentsSummary{
			AllHealthy: true,
			Reason:     core.AllComponentsHealthy,
		}

		got := i.observeComponentsStatus()

		if !reflect.DeepEqual(got, want) {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})
}

func TestObserveNodesStatus(t *testing.T) {
	t.Run("make sure listNodeStatus is called", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		clientSpy := &ClientSpy{}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		_ = i.observeNodesStatus()

		assertWantedCalls(t, []string{listNodeStatus}, clientSpy)
	})

	t.Run("get nodes failed", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		listNodeStatusErr := errors.New("list nodes failed")
		clientSpy := &ClientSpy{listNodeStatusErr: listNodeStatusErr}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := core.NodesSummary{
			AllReady: false,
			Reason:   core.StatusUnknown,
			Message:  listNodeStatusErr.Error(),
		}

		got := i.observeNodesStatus()

		if !reflect.DeepEqual(got, want) {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})

	t.Run("one node not ready", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		clientSpy := &ClientSpy{nodeStatusList: []core.NodeStatus{
			core.NodeStatus{Name: "node1", Ready: true},
			core.NodeStatus{Name: "node2", Ready: false, Message: "node2 is not ready."},
		}}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := core.NodesSummary{
			AllReady: false,
			Reason:   core.SomeNodesNotReady,
			Message:  "node2 are(is) not ready.",
		}

		got := i.observeNodesStatus()

		if !reflect.DeepEqual(got, want) {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})

	t.Run("all nodes ready", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		clientSpy := &ClientSpy{nodeStatusList: []core.NodeStatus{
			core.NodeStatus{Name: "node1", Ready: true},
			core.NodeStatus{Name: "node2", Ready: true},
		}}
		i := StatusInteractor{Log: logger, Client: clientSpy}

		want := core.NodesSummary{AllReady: true, Reason: core.AllNodesReady}

		got := i.observeNodesStatus()

		if !reflect.DeepEqual(got, want) {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})
}

func assertWantedCalls(t *testing.T, wanted []string, spy *ClientSpy) {
	t.Helper()

	if !reflect.DeepEqual(wanted, spy.Calls) {
		t.Errorf("wanted calls %v got %v", wanted, spy.Calls)
	}
}

func assertError(t *testing.T, got error, want error) {
	t.Helper()

	if got == nil {
		t.Fatal("wanted an error but didn't get one")
	}

	if got != want {
		t.Errorf("got %s, want %s", got, want)
	}
}

func assertNoError(t *testing.T, got error) {
	t.Helper()

	if got != nil {
		t.Fatal("didn't want an error but got one")
	}
}
