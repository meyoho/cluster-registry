package usecase

import (
	"fmt"
	"strings"

	"github.com/go-logr/logr"

	core "ake.alauda.io/cluster-registry/domain/entity"
)

const (
	markerNamespace = "kube-public"
	markerKey       = "clusterregistry.k8s.io/imported"
)

type StatusInteractor struct {
	Log    logr.Logger
	Client interface {
		GetVersion() (core.ClusterVersion, error)
		GetNamespaceLabels(namespace string) (map[string]string, error)
		ListComponentStatus() ([]core.ComponentStatus, error)
		ListNodeStatus() ([]core.NodeStatus, error)
		SetNamespaceLabels(namespace string, labels map[string]string) error
	}
}

type StatusInteractorInterface interface {
	CheckImportable(supportedVersions []core.ClusterVersion) (bool, error)
	MarkAsImported() error
	ObserveStatus() ObservedStatus
}

type ObservedStatus struct {
	Accessible bool
	// AccessibleErrMsg will be set when Accessible is false
	AccessibleErrMsg string
	// Version field is only set when Accessible is true, check Accessible first
	Version string
	// ComponentsSummary field is only set when Accessible is true, check Accessible first
	ComponentsSummary core.ComponentsSummary
	// NodesSummary field is only set when Accessible is true, check Accessible first
	NodesSummary core.NodesSummary
}

const (
	ClusterAlreadyImported     = StatusInteractorErr("this cluster is already imported.")
	ClusterVersionNotSupported = StatusInteractorErr("this cluster's version is not supported.")

	clusterNotAccessibleMsg = "cluster is not accessible."
)

type StatusInteractorErr string

func (e StatusInteractorErr) Error() string {
	return string(e)
}

func (i *StatusInteractor) CheckImportable(supportedVersions []core.ClusterVersion) (bool, error) {
	version, err := i.Client.GetVersion()
	if err != nil {
		return false, err
	}

	if !core.IsVersionSupported(version, supportedVersions) {
		return false, ClusterVersionNotSupported
	}

	labels, err := i.Client.GetNamespaceLabels(markerNamespace)
	if err != nil {
		return false, err
	}

	for key, _ := range labels {
		if key == markerKey {
			return false, ClusterAlreadyImported
		}
	}

	return true, nil
}

func (i *StatusInteractor) MarkAsImported() error {
	labels, err := i.Client.GetNamespaceLabels(markerNamespace)
	if err != nil {
		return err
	}

	newLabels := map[string]string{markerKey: ""}
	for key, val := range labels {
		if key != markerKey {
			newLabels[key] = val
		}
	}
	if err = i.Client.SetNamespaceLabels(markerNamespace, newLabels); err != nil {
		return err
	}

	return nil
}

func (i *StatusInteractor) DeleteImportedMarker() error {
	labels, err := i.Client.GetNamespaceLabels(markerNamespace)
	if err != nil {
		return err
	}

	newLabels := map[string]string{}
	for key, val := range labels {
		if key != markerKey {
			newLabels[key] = val
		}
	}
	if err = i.Client.SetNamespaceLabels(markerNamespace, newLabels); err != nil {
		return err
	}

	return nil
}

func (i *StatusInteractor) ObserveStatus() ObservedStatus {
	version, err := i.Client.GetVersion()
	if err != nil {
		return ObservedStatus{
			Accessible:       false,
			AccessibleErrMsg: err.Error(),
			ComponentsSummary: core.ComponentsSummary{
				AllHealthy: false,
				Reason:     core.StatusUnknown,
				Message:    clusterNotAccessibleMsg,
			},
			NodesSummary: core.NodesSummary{
				AllReady: false,
				Reason:   core.StatusUnknown,
				Message:  clusterNotAccessibleMsg,
			},
		}
	}

	return ObservedStatus{
		Accessible:        true,
		Version:           version.String(),
		ComponentsSummary: i.observeComponentsStatus(),
		NodesSummary:      i.observeNodesStatus(),
	}
}

func (i *StatusInteractor) observeComponentsStatus() core.ComponentsSummary {
	css, err := i.Client.ListComponentStatus()
	if err != nil {
		return core.ComponentsSummary{
			AllHealthy: false,
			Reason:     core.StatusUnknown,
			Message:    err.Error(),
		}
	}

	unHealthyComponents := []string{}
	for _, cs := range css {
		if !cs.Healthy {
			unHealthyComponents = append(unHealthyComponents, cs.Name)
		}
	}

	if len(unHealthyComponents) != 0 {
		return core.ComponentsSummary{
			AllHealthy: false,
			Reason:     core.SomeComponentsUnhealthy,
			Message:    fmt.Sprintf("%s are(is) unhealthy.", strings.Join(unHealthyComponents, ",")),
		}
	}

	return core.ComponentsSummary{AllHealthy: true, Reason: core.AllComponentsHealthy}
}

func (i *StatusInteractor) observeNodesStatus() core.NodesSummary {
	nodes, err := i.Client.ListNodeStatus()
	if err != nil {
		return core.NodesSummary{
			AllReady: false,
			Reason:   core.StatusUnknown,
			Message:  err.Error(),
		}
	}

	notReadyNodes := []string{}
	for _, n := range nodes {
		if !n.Ready {
			notReadyNodes = append(notReadyNodes, n.Name)
		}
	}

	if len(notReadyNodes) != 0 {
		return core.NodesSummary{
			AllReady: false,
			Reason:   core.SomeNodesNotReady,
			Message:  fmt.Sprintf("%s are(is) not ready.", strings.Join(notReadyNodes, ",")),
		}
	}

	return core.NodesSummary{AllReady: true, Reason: core.AllNodesReady}
}
