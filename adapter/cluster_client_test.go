package adapter

import (
	"reflect"
	"testing"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/version"

	logrTesting "github.com/go-logr/logr/testing"
	fakeDiscovery "k8s.io/client-go/discovery/fake"
	fakeKubernetes "k8s.io/client-go/kubernetes/fake"
	clientTesting "k8s.io/client-go/testing"

	core "ake.alauda.io/cluster-registry/domain/entity"
)

func TestGetVersion(t *testing.T) {
	t.Run("version is not semantic", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		c := &ClusterClient{
			Log: logger,
			DiscoveryClient: &fakeDiscovery.FakeDiscovery{
				Fake:               &clientTesting.Fake{},
				FakedServerVersion: &version.Info{GitVersion: "v1.13-2"},
			},
		}

		_, err := c.GetVersion()

		assertError(t, err, VersionNotSemantic)
	})

	t.Run("version is semantic", func(t *testing.T) {
		logger := &logrTesting.TestLogger{T: t}
		c := &ClusterClient{
			Log: logger,
			DiscoveryClient: &fakeDiscovery.FakeDiscovery{
				Fake:               &clientTesting.Fake{},
				FakedServerVersion: &version.Info{GitVersion: "v1.13.2"},
			},
		}

		want := core.ClusterVersion{Major: 1, Minor: 13, Patch: 2}

		got, err := c.GetVersion()

		assertNoError(t, err)

		if !reflect.DeepEqual(got, want) {
			t.Errorf("wanted %v but got %v", want, got)
		}
	})
}

func TestGetNamespaceLabels(t *testing.T) {
	logger := &logrTesting.TestLogger{T: t}
	labels := map[string]string{"example-label-key": "value"}
	clientSet := fakeKubernetes.NewSimpleClientset(
		&corev1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "kube-public", Labels: labels}})
	c := &ClusterClient{Log: logger, Clientset: clientSet}

	want := labels

	got, err := c.GetNamespaceLabels("kube-public")

	assertNoError(t, err)

	if !reflect.DeepEqual(got, want) {
		t.Errorf("wanted %v but got %v", want, got)
	}
}

func TestListComponentStatus(t *testing.T) {
	csTests := []struct {
		name            string
		componentStatus []runtime.Object
		wantStatus      []core.ComponentStatus
	}{
		{
			"healthy type is true",
			[]runtime.Object{
				&corev1.ComponentStatus{
					ObjectMeta: metav1.ObjectMeta{Name: "controller-manager"},
					Conditions: []corev1.ComponentCondition{
						corev1.ComponentCondition{Type: "Healthy", Status: corev1.ConditionTrue},
					},
				},
			},
			[]core.ComponentStatus{
				core.ComponentStatus{Name: "controller-manager", Healthy: true},
			},
		},
		{
			"healthy type is false",
			[]runtime.Object{
				&corev1.ComponentStatus{
					ObjectMeta: metav1.ObjectMeta{Name: "scheduler"},
					Conditions: []corev1.ComponentCondition{
						corev1.ComponentCondition{Type: "Healthy", Status: corev1.ConditionFalse, Message: "scheduler unhealthy."},
					},
				},
			},
			[]core.ComponentStatus{
				core.ComponentStatus{Name: "scheduler", Healthy: false, Message: "scheduler unhealthy."},
			},
		},
		{
			"healthy type is unknown",
			[]runtime.Object{
				&corev1.ComponentStatus{
					ObjectMeta: metav1.ObjectMeta{Name: "etcd"},
					Conditions: []corev1.ComponentCondition{
						corev1.ComponentCondition{Type: "Healthy", Status: corev1.ConditionUnknown, Message: "etcd unknown."},
					},
				},
			},
			[]core.ComponentStatus{
				core.ComponentStatus{Name: "etcd", Healthy: false, Message: "etcd unknown."},
			},
		},
		{
			"component status has no conditions",
			[]runtime.Object{
				&corev1.ComponentStatus{
					ObjectMeta: metav1.ObjectMeta{Name: "etcd2"},
				},
			},
			[]core.ComponentStatus{
				core.ComponentStatus{Name: "etcd2", Healthy: false},
			},
		},
	}

	for _, tt := range csTests {
		t.Run(tt.name, func(t *testing.T) {
			logger := &logrTesting.TestLogger{T: t}
			clientSet := fakeKubernetes.NewSimpleClientset(tt.componentStatus...)
			c := &ClusterClient{Log: logger, Clientset: clientSet}

			got, err := c.ListComponentStatus()

			assertNoError(t, err)

			if !reflect.DeepEqual(got, tt.wantStatus) {
				t.Errorf("wanted %v but got %v", tt.wantStatus, got)
			}
		})
	}
}

func TestListNodeStatus(t *testing.T) {
	nsTests := []struct {
		name       string
		nodeObject []runtime.Object
		wantStatus []core.NodeStatus
	}{
		{
			"ready type is true",
			[]runtime.Object{
				&corev1.Node{
					ObjectMeta: metav1.ObjectMeta{Name: "node1"},
					Status: corev1.NodeStatus{Conditions: []corev1.NodeCondition{
						corev1.NodeCondition{Type: "Ready", Status: corev1.ConditionTrue},
					}},
				},
			},
			[]core.NodeStatus{
				core.NodeStatus{Name: "node1", Ready: true},
			},
		},
		{
			"ready type is false",
			[]runtime.Object{
				&corev1.Node{
					ObjectMeta: metav1.ObjectMeta{Name: "node2"},
					Status: corev1.NodeStatus{Conditions: []corev1.NodeCondition{
						corev1.NodeCondition{Type: "Ready", Status: corev1.ConditionFalse, Message: "node2 is not ready."},
					}},
				},
			},
			[]core.NodeStatus{
				core.NodeStatus{Name: "node2", Ready: false, Message: "node2 is not ready."},
			},
		},
		{
			"ready type is unknown",
			[]runtime.Object{
				&corev1.Node{
					ObjectMeta: metav1.ObjectMeta{Name: "node3"},
					Status: corev1.NodeStatus{Conditions: []corev1.NodeCondition{
						corev1.NodeCondition{Type: "Ready", Status: corev1.ConditionUnknown, Message: "node3 is unknown."},
					}},
				},
			},
			[]core.NodeStatus{
				core.NodeStatus{Name: "node3", Ready: false, Message: "node3 is unknown."},
			},
		},
		{
			"node has no conditons",
			[]runtime.Object{
				&corev1.Node{
					ObjectMeta: metav1.ObjectMeta{Name: "node4"},
				},
			},
			[]core.NodeStatus{
				core.NodeStatus{Name: "node4", Ready: false},
			},
		},
	}

	for _, tt := range nsTests {
		t.Run(tt.name, func(t *testing.T) {
			logger := &logrTesting.TestLogger{T: t}
			clientSet := fakeKubernetes.NewSimpleClientset(tt.nodeObject...)
			c := &ClusterClient{Log: logger, Clientset: clientSet}

			got, err := c.ListNodeStatus()

			assertNoError(t, err)

			if !reflect.DeepEqual(got, tt.wantStatus) {
				t.Errorf("wanted %v but got %v", tt.wantStatus, got)
			}
		})
	}
}

func TestSetNamespaceLabels(t *testing.T) {
	logger := &logrTesting.TestLogger{T: t}
	clientSet := fakeKubernetes.NewSimpleClientset(
		&corev1.Namespace{ObjectMeta: metav1.ObjectMeta{
			Name: "kube-public", Labels: map[string]string{"example-label-key": "value"}}})
	newLabels := map[string]string{"new-label-key": "foo"}
	c := &ClusterClient{Log: logger, Clientset: clientSet}

	want := newLabels

	c.SetNamespaceLabels("kube-public", newLabels)
	got, err := c.GetNamespaceLabels("kube-public")

	assertNoError(t, err)

	if !reflect.DeepEqual(got, want) {
		t.Errorf("wanted %s but got %s", want, got)
	}
}

func assertError(t *testing.T, got error, want error) {
	t.Helper()

	if got == nil {
		t.Fatal("wanted an error but didn't get one")
	}

	if got != want {
		t.Errorf("got %s, want %s", got, want)
	}
}

func assertNoError(t *testing.T, got error) {
	t.Helper()

	if got != nil {
		t.Fatal("didn't want an error but got one")
	}
}
