package adapter

import (
	"reflect"
	"testing"

	logrTesting "github.com/go-logr/logr/testing"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	clusterregistryv1alpha1 "ake.alauda.io/cluster-registry/api/v1alpha1"
	core "ake.alauda.io/cluster-registry/domain/entity"
	uc "ake.alauda.io/cluster-registry/domain/usecase"
)

func TestPresentClusterStatus(t *testing.T) {
	current := metav1.Now()
	tests := []struct {
		name           string
		observedStatus uc.ObservedStatus
		want           clusterregistryv1alpha1.ClusterStatus
	}{
		{
			"some nodes not ready",
			uc.ObservedStatus{
				Accessible: true,
				Version:    "v1.13.2",
				ComponentsSummary: core.ComponentsSummary{
					AllHealthy: true,
					Reason:     core.AllComponentsHealthy,
				},
				NodesSummary: core.NodesSummary{
					AllReady: false,
					Reason:   core.SomeNodesNotReady,
					Message:  "node1,node2 are(is) not ready.",
				},
			},
			clusterregistryv1alpha1.ClusterStatus{
				Version: "v1.13.2",
				Conditions: []clusterregistryv1alpha1.ClusterCondition{
					clusterregistryv1alpha1.ClusterCondition{
						Type:               clusterregistryv1alpha1.NotAccessible,
						Status:             corev1.ConditionFalse,
						LastHeartbeatTime:  current,
						LastTransitionTime: current,
					},
					clusterregistryv1alpha1.ClusterCondition{
						Type:               clusterregistryv1alpha1.ComponentNotHealthy,
						Status:             corev1.ConditionFalse,
						Reason:             string(core.AllComponentsHealthy),
						LastHeartbeatTime:  current,
						LastTransitionTime: current,
					},
					clusterregistryv1alpha1.ClusterCondition{
						Type:               clusterregistryv1alpha1.NodeNotReady,
						Status:             corev1.ConditionTrue,
						Reason:             string(core.SomeNodesNotReady),
						Message:            "node1,node2 are(is) not ready.",
						LastHeartbeatTime:  current,
						LastTransitionTime: current,
					},
				},
			},
		},
		{
			"cluster not accessible",
			uc.ObservedStatus{
				Accessible:       false,
				AccessibleErrMsg: "connection reset by peer.",
				ComponentsSummary: core.ComponentsSummary{
					AllHealthy: false,
					Reason:     core.StatusUnknown,
					Message:    "cluster is not accessible.",
				},
				NodesSummary: core.NodesSummary{
					AllReady: false,
					Reason:   core.StatusUnknown,
					Message:  "cluster is not accessible.",
				},
			},
			clusterregistryv1alpha1.ClusterStatus{
				Conditions: []clusterregistryv1alpha1.ClusterCondition{
					clusterregistryv1alpha1.ClusterCondition{
						Type:               clusterregistryv1alpha1.NotAccessible,
						Status:             corev1.ConditionTrue,
						Message:            "connection reset by peer.",
						LastHeartbeatTime:  current,
						LastTransitionTime: current,
					},
					clusterregistryv1alpha1.ClusterCondition{
						Type:               clusterregistryv1alpha1.ComponentNotHealthy,
						Status:             corev1.ConditionUnknown,
						Reason:             string(core.StatusUnknown),
						Message:            "cluster is not accessible.",
						LastHeartbeatTime:  current,
						LastTransitionTime: current,
					},
					clusterregistryv1alpha1.ClusterCondition{
						Type:               clusterregistryv1alpha1.NodeNotReady,
						Status:             corev1.ConditionUnknown,
						Reason:             string(core.StatusUnknown),
						Message:            "cluster is not accessible.",
						LastHeartbeatTime:  current,
						LastTransitionTime: current,
					},
				},
			},
		},
		{
			"components status unknown",
			uc.ObservedStatus{
				Accessible: true,
				Version:    "v1.13.2",
				ComponentsSummary: core.ComponentsSummary{
					AllHealthy: false,
					Reason:     core.StatusUnknown,
					Message:    "list components status failed.",
				},
				NodesSummary: core.NodesSummary{
					AllReady: true,
					Reason:   core.AllNodesReady,
				},
			},
			clusterregistryv1alpha1.ClusterStatus{
				Version: "v1.13.2",
				Conditions: []clusterregistryv1alpha1.ClusterCondition{
					clusterregistryv1alpha1.ClusterCondition{
						Type:               clusterregistryv1alpha1.NotAccessible,
						Status:             corev1.ConditionFalse,
						LastHeartbeatTime:  current,
						LastTransitionTime: current,
					},
					clusterregistryv1alpha1.ClusterCondition{
						Type:               clusterregistryv1alpha1.ComponentNotHealthy,
						Status:             corev1.ConditionUnknown,
						Reason:             string(core.StatusUnknown),
						Message:            "list components status failed.",
						LastHeartbeatTime:  current,
						LastTransitionTime: current,
					},
					clusterregistryv1alpha1.ClusterCondition{
						Type:               clusterregistryv1alpha1.NodeNotReady,
						Status:             corev1.ConditionFalse,
						Reason:             string(core.AllNodesReady),
						LastHeartbeatTime:  current,
						LastTransitionTime: current,
					},
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logger := &logrTesting.TestLogger{T: t}

			got := PresentClusterStatus(logger, tt.observedStatus, current)

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("wanted %v but got %v", tt.want, got)
			}
		})
	}
}
