package adapter

import (
	"k8s.io/apimachinery/pkg/util/version"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/kubernetes"

	"github.com/go-logr/logr"

	core "ake.alauda.io/cluster-registry/domain/entity"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ClusterClient struct {
	Log logr.Logger

	DiscoveryClient discovery.DiscoveryInterface
	Clientset       kubernetes.Interface
}

const (
	VersionNotSemantic = ClusterClientErr("version is not semantic.")

	healthyConditionType = "Healthy"
	readyConditionType   = "Ready"
)

type ClusterClientErr string

func (c ClusterClientErr) Error() string {
	return string(c)
}

func (c *ClusterClient) GetVersion() (core.ClusterVersion, error) {
	v, err := c.DiscoveryClient.ServerVersion()
	if err != nil {
		return core.ClusterVersion{}, err
	}

	parsedV, err := version.ParseSemantic(v.String())
	if err != nil {
		c.Log.Error(err, "parse version failed", "version", v.String())
		return core.ClusterVersion{}, VersionNotSemantic
	}

	return core.ClusterVersion{
		Major: parsedV.Major(), Minor: parsedV.Minor(), Patch: parsedV.Patch()}, nil
}

func (c *ClusterClient) GetNamespaceLabels(namespace string) (map[string]string, error) {
	ns, err := c.Clientset.CoreV1().Namespaces().Get(namespace, metav1.GetOptions{})
	if err != nil {
		return map[string]string{}, err
	}

	return ns.Labels, nil
}

func (c *ClusterClient) ListComponentStatus() ([]core.ComponentStatus, error) {
	csList, err := c.Clientset.CoreV1().ComponentStatuses().List(metav1.ListOptions{})
	if err != nil {
		return []core.ComponentStatus{}, err
	}

	result := []core.ComponentStatus{}
	for _, cs := range csList.Items {
		healthy := false
		message := ""
		for _, c := range cs.Conditions {
			if c.Type == healthyConditionType {
				message = c.Message
				if c.Status == corev1.ConditionTrue {
					healthy = true
				}
			}
		}
		result = append(result,
			core.ComponentStatus{
				Name:    cs.ObjectMeta.Name,
				Healthy: healthy,
				Message: message,
			},
		)
	}

	return result, nil
}

func (c *ClusterClient) ListNodeStatus() ([]core.NodeStatus, error) {
	nList, err := c.Clientset.CoreV1().Nodes().List(metav1.ListOptions{})
	if err != nil {
		return []core.NodeStatus{}, err
	}

	result := []core.NodeStatus{}
	for _, n := range nList.Items {
		ready := false
		message := ""
		for _, c := range n.Status.Conditions {
			if c.Type == readyConditionType {
				message = c.Message
				if c.Status == corev1.ConditionTrue {
					ready = true
				}
			}
		}

		result = append(result,
			core.NodeStatus{
				Name:    n.ObjectMeta.Name,
				Ready:   ready,
				Message: message,
			},
		)
	}

	return result, nil
}

func (c *ClusterClient) SetNamespaceLabels(namespace string, labels map[string]string) error {
	ns, err := c.Clientset.CoreV1().Namespaces().Get(namespace, metav1.GetOptions{})
	if err != nil {
		return err
	}

	ns = ns.DeepCopy()
	ns.ObjectMeta.Labels = labels
	_, err = c.Clientset.CoreV1().Namespaces().Update(ns)
	if err != nil {
		return err
	}

	return nil
}
