package adapter

import (
	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	clusterregistryv1alpha1 "ake.alauda.io/cluster-registry/api/v1alpha1"
	core "ake.alauda.io/cluster-registry/domain/entity"
	uc "ake.alauda.io/cluster-registry/domain/usecase"
)

func PresentClusterStatus(logger logr.Logger, status uc.ObservedStatus, current metav1.Time) clusterregistryv1alpha1.ClusterStatus {
	accessibleCond := clusterregistryv1alpha1.ClusterCondition{
		Type:               clusterregistryv1alpha1.NotAccessible,
		Status:             corev1.ConditionFalse,
		LastHeartbeatTime:  current,
		LastTransitionTime: current,
	}
	if !status.Accessible {
		accessibleCond.Status = corev1.ConditionTrue
		accessibleCond.Message = status.AccessibleErrMsg
	}

	componentCond := clusterregistryv1alpha1.ClusterCondition{
		Type:               clusterregistryv1alpha1.ComponentNotHealthy,
		Status:             corev1.ConditionFalse,
		Reason:             string(status.ComponentsSummary.Reason),
		Message:            status.ComponentsSummary.Message,
		LastHeartbeatTime:  current,
		LastTransitionTime: current,
	}
	if !status.ComponentsSummary.AllHealthy {
		if status.ComponentsSummary.Reason == core.StatusUnknown {
			componentCond.Status = corev1.ConditionUnknown
		} else {
			componentCond.Status = corev1.ConditionTrue
		}
	}

	nodeCond := clusterregistryv1alpha1.ClusterCondition{
		Type:               clusterregistryv1alpha1.NodeNotReady,
		Status:             corev1.ConditionFalse,
		Reason:             string(status.NodesSummary.Reason),
		Message:            status.NodesSummary.Message,
		LastHeartbeatTime:  current,
		LastTransitionTime: current,
	}
	if !status.NodesSummary.AllReady {
		if status.NodesSummary.Reason == core.StatusUnknown {
			nodeCond.Status = corev1.ConditionUnknown
		} else {
			nodeCond.Status = corev1.ConditionTrue
		}
	}

	return clusterregistryv1alpha1.ClusterStatus{
		Version: status.Version,
		Conditions: []clusterregistryv1alpha1.ClusterCondition{
			accessibleCond, componentCond, nodeCond,
		},
	}
}
