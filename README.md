<h1 align="center"> Cluster Registry Manager </h1>
<p align="center">
The Lost Implemention of cluster-registry Spec
</p>
<p align="center">
  <img src="https://img.shields.io/badge/unit%20test%20coverage-91.8%25-blue.svg">
  <img src="https://travis-ci.com/youyongsong/cluster-registry-manager.svg?token=Sw5rRsVnRVuxypxxR9oR&branch=master">
</p>

<!-- MarkdownTOC GFM -->
- [特性](#%E7%89%B9%E6%80%A7)
- [文档](#%E6%96%87%E6%A1%A3)
- [快速部署](#%E5%BF%AB%E9%80%9F%E9%83%A8%E7%BD%B2)
- [API 示例](#API-%E7%A4%BA%E4%BE%8B)
- [参与方式](#%E5%8F%82%E4%B8%8E%E6%96%B9%E5%BC%8F)
  - [项目维护人员](#%E9%A1%B9%E7%9B%AE%E7%BB%B4%E6%8A%A4%E4%BA%BA%E5%91%98)
- [开发贡献](#%E5%BC%80%E5%8F%91%E8%B4%A1%E7%8C%AE)
<!-- /MarkdownTOC -->

---

## 特性
* 扩展了 Cluster Registry API 中的状态字段，可以展示更丰富的集群状态
* 实现了 Cluster Registry Controller 用于更新集群资源的状态
* 实现了 Cluster Registry Admission Webhook 用于对集群资源进行校验

## 文档
目前项目文档结构还在建立中，你可以在 [wiki](https://github.com/youyongsong/cluster-registry-manager/wiki) 中找到一些零散的文档。

## 快速部署

## API 示例
Cluster 资源创建示例:
```yaml
apiVersion: clusterregistry.k8s.io/v1alpha1
kind: Cluster
metadata:
  name: cluster-sample
spec:
  authInfo:
    controller:
      namespace: default
      name: cluster-sample-token
      kind: Secret
  kubernetesApiEndpoints:
    serverEndpoints:
      - serverAddress: "https://<k8s-cluster-ip>"
    caBundle: <caBundle-data>
---
apiVersion: v1
kind: Secret
metadata:
  name: cluster-sample-token
  namespace: default
type: Opaque
data:
  token: <token-base64-data>  # This token is used by cluster-registry-controller to observe cluster's status
```
Cluster 资源获取示例:
```yaml
apiVersion: clusterregistry.k8s.io/v1alpha1
kind: Cluster
metadata:
  name: cluster-sample
spec:
  authInfo:
    controller:
      namespace: default
      name: cluster-sample-token
      kind: Secret
  kubernetesApiEndpoints:
    serverEndpoints:
      - serverAddress: "https://<k8s-cluster-ip>"
    caBundle: <caBundle-data>
status:
  version: v1.13.4
  Conditions:
    - type: NotAccessible
      status: "False"
      reason: ""
      message: ""
      lastHeartbeatTime: "2019-06-05T07:56:26Z"
      lastTransitionTimei: "2019-04-24T02:43:05Z"
    - type: ComponentNotHealthy
      status: "False"
      reason: "all_components_healthy"
      message: ""
      lastHeartbeatTime: "2019-06-05T07:56:26Z"
      lastTransitionTimei: "2019-04-24T02:43:05Z"
    - type: NotAccessible
      status: "False"
      reason: "all_nodes_ready"
      message: ""
      lastHeartbeatTime: "2019-06-05T07:56:26Z"
      lastTransitionTimei: "2019-04-24T02:43:05Z"
```

## 参与方式
cluster-registry-manager 项目还处在初期阶段，但我们非常欢你迎加入 [Slack #dev-cluster-registry](https://ake-project.slack.com/messages/CJV4779NV/) 频道和我们讨论任何项目相关的问题！
### 项目维护人员
* [@nashasha1](https://github.com/nashasha1)
* [@ysyou](https://github.com/youyongsong)

## 开发贡献
项目处在内部开发的阶段，开发贡献相关的文档可以在 [wiki](https://github.com/youyongsong/cluster-registry-manager/wiki) 中找到。
