# Global Args
ARG APP_BASE=gcr.io/distroless/static:nonroot

ARG GOLANG_VERSION=1.13
FROM golang:${GOLANG_VERSION} as golang

# -----------------------------------------------

# Base Image for test, build and dev
FROM golang as base
LABEL stage=base

# Install tools
WORKDIR /download

# Install kustomize
ARG KUSTOMIZE_DOWNLOAD_URL="https://github.com/kubernetes-sigs/kustomize/releases/download/v2.0.3/kustomize_2.0.3_linux_amd64"
RUN wget ${KUSTOMIZE_DOWNLOAD_URL} -O kustomize && \
    chmod +x kustomize && mv kustomize /usr/local/bin

# Install kubebuilder kube-apiserver etcd
ARG KUBEBUILDER_DOWNLOAD_URL="https://github.com/kubernetes-sigs/kubebuilder/releases/download/v2.0.0-alpha.4/kubebuilder_2.0.0-alpha.4_linux_amd64.tar.gz"
RUN wget ${KUBEBUILDER_DOWNLOAD_URL} -O kubebuilder.tar.gz && \
    mkdir kubebuilder && mkdir -p /usr/local/kubebuilder/bin && \
    tar -xzvf kubebuilder.tar.gz -C kubebuilder --strip-components 1 && \
    mv kubebuilder/bin/* /usr/local/kubebuilder/bin
ENV PATH=${PATH}:/usr/local/kubebuilder/bin

# Install helm
ARG HELM_DOWNLOAD_URL="https://get.helm.sh/helm-v2.14.1-linux-amd64.tar.gz"
RUN wget ${HELM_DOWNLOAD_URL} -O helm.tar.gz && \
    mkdir helm && tar -xzvf helm.tar.gz -C helm --strip-components 1 && \
    mv helm/helm /usr/local/bin

# Set workspace
ENV GO111MODULE=on
WORKDIR /workspace

# -----------------------------------------------

# Run checks and tests
FROM base as ci
LABEL stage=ci

# Install go packages and Copy go source
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

COPY main.go main.go
COPY api/ api/
COPY controllers/ controllers/
COPY webhooks/ webhooks/
COPY domain/ domain/
COPY adapter/ adapter/
COPY util/ util/

# Copy configs
COPY config/ config/

# format check
RUN bash -c "diff -u <(echo -n) <(gofmt -d ./)"

# static check
RUN go vet

# unit test
RUN go test ./domain/... ./adapter/... ./util/...

# integration test
RUN go test ./api/... ./controllers/... ./webhooks/...

# -----------------------------------------------

# Build the manager binary and manifests
FROM base as builder
LABEL stage=build

# Cache go packages
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

# Copy and Build manager binary
COPY main.go main.go
COPY api/ api/
COPY controllers/ controllers/
COPY webhooks/ webhooks/
COPY domain/ domain/
COPY adapter/ adapter/
COPY util/ util/

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -o manager main.go

# Copy and Build manifests
COPY config/ config/
COPY hack/helmify.sh hack/helmify.sh
COPY Makefile Makefile

RUN mkdir dist && \
    make manifests && \
    make helmify && \
    kustomize build config/overlays/prod > dist/cluster-registry.yaml && \
    cd charts && tar -czvf ../dist/cluster-registry-chart.tgz cluster-registry

# -----------------------------------------------

# Release artifacts
FROM python:3.7-alpine as draft-release
LABEL stage=draft-release
ARG TAG
ARG GITHUB_TOKEN

COPY hack/requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY hack/draft_release.py draft_release.py

COPY --from=builder /workspace/dist/cluster-registry.yaml .
COPY --from=builder /workspace/dist/cluster-registry-chart.tgz .

# Create a github release with artifacts
ARG CACHEBUST
# The following steps should never be cached.
RUN ./draft_release.py cluster-registry.yaml cluster-registry-chart.tgz --tag ${TAG}

# -----------------------------------------------

# Application
FROM ${APP_BASE}
ARG VERSION
ARG SHA
LABEL version=${VERSION}
LABEL git_sha=${SHA}
WORKDIR /
COPY --from=builder /workspace/manager .
USER nonroot:nonroot
ENTRYPOINT ["/manager"]
