#!/usr/bin/env bash

local_ip=$1
ca_bundle=$2

cat << EOF
apiVersion: admissionregistration.k8s.io/v1beta1
kind: ValidatingWebhookConfiguration
metadata:
  creationTimestamp: null
  name: validating-webhook-configuration
webhooks:
- clientConfig:
      url: https://${local_ip}:9443/validate-cluster
      caBundle: ${ca_bundle}
  failurePolicy: Fail
  name: validate-cluster.clusterregistry.k8s.io
  rules:
  - apiGroups:
    - clusterregistry.k8s.io
    apiVersions:
    - v1alpha1
    operations:
    - CREATE
    - DELETE
    resources:
    - clusters
EOF