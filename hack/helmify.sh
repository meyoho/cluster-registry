#!/usr/bin/env bash

dest_base=charts
kustomization=config/overlays/chart

cmd=$1
version=$2
appVersion=$3
chart_name=cluster-registry
dest=$dest_base/cluster-registry

chart=${chart/.\//}

build() {
  if [ ! -d "$dest_base" ]; then
    mkdir $dest_base
  fi
  mkdir -p $dest/templates

  echo "generating $dest/Chart.yaml" 1>&2

  cat <<EOF > $dest/Chart.yaml
apiVersion: v1
appVersion: "${appVersion}"
description: A Helm chart for Kubernetes
name: $chart_name
version: ${version}
EOF

  echo "generating $dest/templates/NOTES.txt" 1>&2

  cat <<EOF > $dest/templates/NOTES.txt
$chart has been installed as release {{ .Release.Name }}.

Run \`helm status {{ .Release.Name }}\` for more information.
Run \`helm delete --purge {{.Release.Name}}\` to uninstall.
EOF

  echo "generating $dest/values.yaml" 1>&2

  cat <<EOF > $dest/values.yaml
global:
  namespace: cluster-registry-system
  registry:
    address: index.alauda.cn
  images:
    cluster_registry:
      repository: claas/cluster-registry
      tag: v0.2
    kube_rbac_proxy:
      repository: gcr.io/kubebuilder/kube-rbac-proxy
      tag: v0.4.0
resources:
  requests:
    cpu: 300m
    memory: 200Mi
  limits:
    cpu: 600m
    memory: 600Mi
EOF

  echo "running kustomize" 1>&2

  kustomize build $kustomization > $dest/templates/all.yaml

  echo "running helm lint" 1>&2

  helm lint $dest
}

clean() {
  rm -rf $dest_base
}

case "$cmd" in
  "build" ) build ;;
  "clean" ) clean ;;
  * ) echo "unsupported command: $cmd" 1>&2; exit 1 ;;
esac