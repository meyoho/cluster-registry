#!/usr/bin/env python
import os
import sys

import click
from github import Github

TOKEN_ENV = "GITHUB_TOKEN"
REPO_NAME = "youyongsong/cluster-registry-manager"
RELEASE_MESSAGE_TEMPLATE = """\
This is a draft release auto created by CI/CD workflow.
Please do the following tasks to publish a formal release:
- [ ] Test this release's assets.
- [ ] Run e2e and manual tests for this tag.
- [ ] Replace this message with your formal release notes.
- [ ] Promote this draft release to pre-release or formal release.
"""

@click.command()
@click.argument("assets", required=True, nargs=-1, type=click.Path(exists=True))
@click.option("--tag", required=True, help="Tag name of the target release.")
def draft_release(assets, tag):
    token = os.getenv(TOKEN_ENV)
    if not token:
        sys.exit(f"Environment Var {TOKEN_ENV} not set!")

    g = Github(token)
    repo = g.get_repo(REPO_NAME)
    try:
        release = repo.create_git_release(
            tag, name=f"[DRAFT] {tag}", message=RELEASE_MESSAGE_TEMPLATE,
            draft=True
        )
    except Exception as e:
        print(f"Release get failed: {e}")
        sys.exit("Release get failed!")
    for asset in assets:
        release.upload_asset(asset)

    print("All assets uploaded succeed!")

if __name__ == "__main__":
    draft_release()