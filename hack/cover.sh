#!/usr/bin/env bash

README_DIR=$1

if [ -d "cover" ]; then
  rm -r cover
fi
mkdir cover

PKG_LIST=$(go list ./domain/... ./adapter/... ./util/...)
for package in ${PKG_LIST}; do
  go test -covermode=count -coverprofile "cover/${package##*/}.cov" "$package" ;
done
echo "mode: count" > cover/coverage.cov
tail -q -n +2 cover/*.cov >> cover/coverage.cov

echo "generating coverage html"
go tool cover -html=cover/coverage.cov -o cover/coverage.html

echo "generating coverage result"
result=$(go tool cover -func=cover/coverage.cov | tail -n 1 | awk 'BEGIN{FS=" "} {print $3}' | sed -e "s/%$//")

echo "modifying coverage badge in README.md"
sed -i "" "s/coverage-.*%25/coverage-${result}%25/g" $README_DIR