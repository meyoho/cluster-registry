#!/usr/bin/env bash

repo=$1
branch=$2
sha=$3

master_branch_name="master"
release_branch_regexp="^release-([0-9]+\.[0-9]+)$"
release_tag_regexp="^v([0-9]+\.[0-9]+)\.([0-9]+)$"

build() {
    arg_version=$1
    tags="${@:2}"

    echo "Building local image $repo:local"
    buildctl build \
      --frontend dockerfile.v0 --local context=. --local dockerfile=. \
      --opt build-arg:VERSION=${arg_version} --opt build-arg:SHA=$sha \
      --output type=docker,name=$repo:local | docker load

    for t in $tags
    do
      echo "Tagging and pushing image $repo:$t"
      docker tag $repo:local $repo:$t
      docker push $repo:$t
    done
}

master_branch() {
  echo "Building image for master branch."
  echo "Will tag images with latest, $sha"

  build latest latest $sha
}

release_branch() {
  echo "Building image for release branch $branch ."
  echo "Will tag images with ${base_version}-latest, $sha"

  build ${base_version} ${sha} "${base_version}-latest"
}

release_tag() {
  echo "Building image for release tag $branch."
  echo "Will tag images with ${base_version}, ${full_version}, $sha"

  build ${full_version} ${sha} ${full_version} ${base_version}
}

if [[ $branch = $master_branch_name ]]; then
  master_branch
elif [[ $branch =~ $release_branch_regexp ]]; then
  base_version="v${BASH_REMATCH[1]}"
  release_branch
elif [[ $branch =~ $release_tag_regexp ]]; then
  base_version="v${BASH_REMATCH[1]}"
  full_version="${base_version}.${BASH_REMATCH[2]}"
  release_tag
else
  echo "$branch is unsupported branch, build nothing."
fi
